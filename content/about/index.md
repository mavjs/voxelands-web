+++
date = "2016-04-20T00:00:00"
draft = false
title = "About"
+++

Voxelands is a sandbox construction game inspired by earlier "voxel world" style games such as Minetest, Minecraft, and Infiniminer. Gameplay puts players in a fully destructible 3D game world where they can place and dig numerous types of blocks, items and creatures using a variety of tools. Inside the game world players can build structures, artworks and anything else their creativity can think of on multiplayer servers and singleplayer worlds across multiple game modes.

{{< figure src="../img/survival-small.png" class="img-responsive pull-left media-object" >}} Voxelands was originally forked from the Minetest-C55 0.3.x codebase as a result of several core developers becoming disillusioned with the direction of Minetest 0.4. We wanted to put the fun back into the game, and stop trying to make it into an engine.

["minetest_game's only purpose now is to serve as a base for people who like to mod the hell out of something"](http://irc.minetest.ru/minetest-dev/2014-01-06#i_3536010). *Celeron55 regarding updates to Minetest 0.4's default game*.

<br style="clear: both;"/>

{{< figure src="../img/farming-small.png" class="img-responsive pull-right media-object" >}}

With this in mind the aim was to start with the last stable public release of the 0.3 series, backport bugfixes and improvements from 0.4, and fix some other bugs that were never fixed. Then add in some new features and make a polished release.

Initial changes were a few extra block types and some anti-griefing measures, while primarily reworking the codebase into something usable. This was soon followed by larger changes to the game, including a snow biome, farming, electric circuits - with pistons, lamps, and logic gates - several new draw types - including diagonal fences, walls, fire, and proper sloped roofs - books that can be written in or display craft guides, 3D players and mobs, a newly designed game ui, and much much more.

{{< figure src="../img/circuits-small.png" class="img-responsive pull-left media-object" >}}

Voxelands takes some inspiration from role-playing games, and therefore tries to keep in-game action in-game, rather than having special commands that occur outside of the world. This has lead to things such as home flags, incinerators, and throwable "teleport powder". In the same vein, a character creator has been added with several million variations built-in that allows players to customise how they look to others, while several hundred craftable clothing items allows the player's looks - and defense against creature attacks, cold weather, drowning, and the vacuum of space - to be easily changed inside the game world.

<br style="clear: both;"/>

{{< figure src="../img/craftguide-small.png" class="img-responsive pull-right media-object" >}}

There's also several craftable books inside the game world, these are not only for writting in, but also offer crafting and cooking guides so that players don't need to break from the game to check external websites or wiki's.

In late 2014 the mob system was completely rewritten, and is regularly being improved upon to add many creatures to the game world, adding both additional sources of crafting items and food, and more survival elements from the more dangerous mobs that appear alongside their passive counterparts.

<br style="clear: both;"/>

{{< figure src="../img/village-small.png" class="img-responsive pull-left media-object" >}}

As it stands now, Voxelands has almost 500 types of blocks, plus several hundred craft items, tools, and clothes, along with dozens of mobs. Some are decorative, others structural, and more are interactive; allowing for a great variety of construction to take place, while players work with the natural world along with farmed and crafted items to defend themselves and their ever growing bases, cities, and empires.

So what's next? More mobs, more blocks, more options in the character creator, more improvements to the look and feel of the game, and most importantly; more fun!
