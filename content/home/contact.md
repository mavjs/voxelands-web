+++
date = "2016-04-20T00:00:00"
draft = false
title = "contact"
weight = 0
header = "Join the Community!"
+++

Forum: [Forum.Voxelands.com](http://forum.voxelands.com/)

IRC: [irc.entropynet.net #voxelands](irc://irc.entropynet.net/voxelands)

Google+: [Voxelands Google Plus Community](https://plus.google.com/u/0/communities/104291512236360607560)

Twitter: [@voxelands](https://twitter.com/voxelands)

Facebook: [Voxelands on Facebook](https://www.facebook.com/voxelands)
