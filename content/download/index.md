+++
date = "2016-04-20T00:00:00"
title = "Download"
version = "1608.01"
arch = "https://aur.archlinux.org/packages/voxelands/"
fedora = "http://copr.fedoraproject.org/coprs/mavjs/voxelands/"
chakra = "http://www.chakraos.org/ccr/packages.php?ID=6706"
slackware = "http://slackbuilds.org/repository/14.1/games/voxelands/"
translations = "All Voxelands downloads include translations for English, Japanese, Brazillian Portuguese, Russian, German, Dutch, Italian and partially for Lojban, Polish, Spanish, Romanian, Vietnamese and French."
notice = "Builds for Apple devices will no longer be provided or supported, due to Apple Inc. violating Voxelands' copyright."
donation = "If everyone that downloaded Voxelands gave just $1 a year, it would cover our operating costs and pay for more servers. Care to help?"
+++
